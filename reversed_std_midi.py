#!/usr/bin/env python2.7


''' 
Takes the standard deviation of x, y, z at each datatime
Gets the mins and maxes of those standard deviations
splits the mins and maxes into a certain number of sections

attempt at an explanation to an audience, probably not the best but its a start: 
As the particles disperse after the collision, the pitch goes up/down (depending on if you're using the reversed or regular)
based on the standard deviation of the location of particles at a certain point in time



If you have any questions you can text or email me, most likely the only thing you'll want to change is scale in the makeMidiNotes function
'''
import sys
import os
import math
import numpy as np
from miditime.miditime import MIDITime

# get the location data from the file
def getData(filename):
    datatimeStarted = False
    myFile = open('data/' + filename + '.speck')
    data = {}
    first = True
    for line in myFile:
        if not line.split():
            continue
        if line.split()[0] == 'datatime':
            if not first and len(data[currDataTime]['x']) != 0:
                data[currDataTime]['xAvg'] = np.std(data[currDataTime]['x'])
                data[currDataTime]['yAvg'] = np.std(data[currDataTime]['y'])
                data[currDataTime]['zAvg'] = np.std(data[currDataTime]['z'])
            first = False
            # start new datatime and initialize values
            datatimeStarted = True
            currDataTime = int(line.split()[1])
            data[currDataTime] = {}
            data[currDataTime]['x'] = np.array([])
            data[currDataTime]['y'] = np.array([])
            data[currDataTime]['z'] = np.array([])
            data[currDataTime]['xAvg'] = 0
            data[currDataTime]['yAvg'] = 0
            data[currDataTime]['zAvg'] = 0
            continue
        if not datatimeStarted or line.split()[0] == '#':
            continue
        currLine = line.rstrip('\n').split()
        x = float(currLine[0])
        y = float(currLine[1])
        z = float(currLine[2])
        data[currDataTime]['x'] = np.append(data[currDataTime]['x'],x)
        data[currDataTime]['y'] = np.append(data[currDataTime]['y'],y)
        data[currDataTime]['z'] = np.append(data[currDataTime]['z'],z)

    # get standard deviation and append midi by last datatime
    if not first and len(data[currDataTime]['x']) != 0:
        data[currDataTime]['xAvg'] = np.std(data[currDataTime]['x'])
        data[currDataTime]['yAvg'] = np.std(data[currDataTime]['y'])
        data[currDataTime]['zAvg'] = np.std(data[currDataTime]['z'])
    myFile.close()
    return data

# generator to give floats based on a jump
def frange(x, y, jump):
    while x <= y:
        yield x
        x += jump


# function that splits the list of mins and maxes into the zones
def getZoneSplits(myList):
    # get ranges for each axis
    xRange = [myList[0], myList[1]]
    yRange = [myList[2], myList[3]]
    zRange = [myList[4], myList[5]]
    splits = {'x':[],'y':[],'z':[]}

    # break the x, y, and z up
    for x in frange(xRange[0], xRange[1], (abs(xRange[0]) + abs(xRange[1]))/16):
        splits['x'].append(x)

    for x in frange(yRange[0], yRange[1], (abs(yRange[0]) + abs(yRange[1]))/16):
        splits['y'].append(x)

    for x in frange(zRange[0], zRange[1], (abs(zRange[0]) + abs(zRange[1]))/16):
        splits['z'].append(x)

    return splits

# gets the min and max of the standard deviations of the x, y, and z
def getStdMinMax(data):
    xmin=xmax=ymin=ymax=zmin=zmax = 0
    for dt in data.keys():
        if data[dt]['xAvg'] < xmin:
            xmin = data[dt]['xAvg']
        elif data[dt]['xAvg'] > xmax:
            xmax = data[dt]['xAvg']
        elif data[dt]['yAvg'] < ymin:
            ymin = data[dt]['yAvg']
        elif data[dt]['yAvg'] > ymax:
            ymax = data[dt]['yAvg']
        elif data[dt]['zAvg'] < zmin:
            zmin = data[dt]['zAvg']
        elif data[dt]['zAvg'] > zmax:
            zmax = data[dt]['zAvg']
    return [xmin, xmax, ymin, ymax, zmin, zmax]





'''
assigning notes
ex: midiNotes.append([beat, note, velocity, noteLength])

to change the scale just change the note value in each line
'''
def makeMidiNotes(splits, data, filename):
    # 120 beats per minute
    myMidi = MIDITime(120, 'music_reversed/' + filename + '.mid')
    midiNotes = []
    bt = 0
    nl = .125
    for dt in data.keys():
        if data[dt]['xAvg'] < splits['x'][1] and data[dt]['xAvg'] > splits['x'][0]:
            midiNotes.append([bt, 72, 127, nl])
        elif data[dt]['xAvg'] < splits['x'][2] and data[dt]['xAvg'] > splits['x'][1]:
            midiNotes.append([bt, 70, 120, nl])
        elif data[dt]['xAvg'] < splits['x'][3] and data[dt]['xAvg'] > splits['x'][2]:
            midiNotes.append([bt, 67, 112, nl])
        elif data[dt]['xAvg'] < splits['x'][4] and data[dt]['xAvg'] > splits['x'][3]:
            midiNotes.append([bt, 65, 104, nl])
        elif data[dt]['xAvg'] < splits['x'][5] and data[dt]['xAvg'] > splits['x'][4]:
            midiNotes.append([bt, 63, 96, nl])
        elif data[dt]['xAvg'] < splits['x'][6] and data[dt]['xAvg'] > splits['x'][5]:
            midiNotes.append([bt, 60, 88, nl])
        elif data[dt]['xAvg'] < splits['x'][7] and data[dt]['xAvg'] > splits['x'][6]:
            midiNotes.append([bt, 58, 80, nl])
        elif data[dt]['xAvg'] < splits['x'][8] and data[dt]['xAvg'] > splits['x'][7]:
            midiNotes.append([bt, 55, 72, nl])
        elif data[dt]['xAvg'] < splits['x'][9] and data[dt]['xAvg'] > splits['x'][8]:
            midiNotes.append([bt, 53, 64, nl])
        elif data[dt]['xAvg'] < splits['x'][10] and data[dt]['xAvg'] > splits['x'][9]:
            midiNotes.append([bt, 51, 56, nl])
        elif data[dt]['xAvg'] < splits['x'][11] and data[dt]['xAvg'] > splits['x'][10]:
            midiNotes.append([bt, 36, 48, nl])
        elif data[dt]['xAvg'] < splits['x'][12] and data[dt]['xAvg'] > splits['x'][11]:
            midiNotes.append([bt, 34, 40, nl])
        elif data[dt]['xAvg'] < splits['x'][13] and data[dt]['xAvg'] > splits['x'][12]:
            midiNotes.append([bt, 31, 32, nl])
        elif data[dt]['xAvg'] < splits['x'][14] and data[dt]['xAvg'] > splits['x'][13]:
            midiNotes.append([bt, 29, 24, nl])
        elif data[dt]['xAvg'] < splits['x'][15] and data[dt]['xAvg'] > splits['x'][14]:
            midiNotes.append([bt, 27, 16, nl])
        elif data[dt]['xAvg'] > splits['x'][15]:
            midiNotes.append([bt, 24, 8, nl])
        else:
            midiNotes.append([bt, 67, 1, nl])

        #offset y note pitches by 4 compared to x for a major third interval(start at E)

        if data[dt]['yAvg'] < splits['y'][1] and data[dt]['yAvg'] > splits['y'][0]:
            midiNotes.append([bt, 87, 127, nl])
        elif data[dt]['yAvg'] < splits['y'][2] and data[dt]['yAvg'] > splits['y'][1]:
            midiNotes.append([bt, 84, 120, nl])
        elif data[dt]['yAvg'] < splits['y'][3] and data[dt]['yAvg'] > splits['y'][2]:
            midiNotes.append([bt, 82, 112, nl])
        elif data[dt]['yAvg'] < splits['y'][4] and data[dt]['yAvg'] > splits['y'][3]:
            midiNotes.append([bt, 79, 104, nl])
        elif data[dt]['yAvg'] < splits['y'][5] and data[dt]['yAvg'] > splits['y'][4]:
            midiNotes.append([bt, 77, 96, nl])
        elif data[dt]['yAvg'] < splits['y'][6] and data[dt]['yAvg'] > splits['y'][5]:
            midiNotes.append([bt, 75, 88, nl])
        elif data[dt]['yAvg'] < splits['y'][7] and data[dt]['yAvg'] > splits['y'][6]:
            midiNotes.append([bt, 72, 80, nl])
        elif data[dt]['yAvg'] < splits['y'][8] and data[dt]['yAvg'] > splits['y'][7]:
            midiNotes.append([bt, 70, 72, nl])
        elif data[dt]['yAvg'] < splits['y'][9] and data[dt]['yAvg'] > splits['y'][8]:
            midiNotes.append([bt, 67, 64, nl])
        elif data[dt]['yAvg'] < splits['y'][10] and data[dt]['yAvg'] > splits['y'][9]:
            midiNotes.append([bt, 53, 56, nl])
        elif data[dt]['yAvg'] < splits['y'][11] and data[dt]['yAvg'] > splits['y'][10]:
            midiNotes.append([bt, 51, 48, nl])
        elif data[dt]['yAvg'] < splits['y'][12] and data[dt]['yAvg'] > splits['y'][11]:
            midiNotes.append([bt, 48, 40, nl])
        elif data[dt]['yAvg'] < splits['y'][13] and data[dt]['yAvg'] > splits['y'][12]:
            midiNotes.append([bt, 46, 32, nl])
        elif data[dt]['yAvg'] < splits['y'][14] and data[dt]['yAvg'] > splits['y'][13]:
            midiNotes.append([bt, 43, 24, nl])
        elif data[dt]['yAvg'] < splits['y'][15] and data[dt]['yAvg'] > splits['y'][14]:
            midiNotes.append([bt, 41, 16, nl])
        elif data[dt]['yAvg'] > splits['y'][15]:
            midiNotes.append([bt, 39, 8, nl])
        else:
            midiNotes.append([bt, 67, 1, nl])

        #z values start at G
        if data[dt]['zAvg'] < splits['z'][1] and data[dt]['zAvg'] > splits['z'][0]:
            midiNotes.append([bt, 103, 127  , nl])
        elif data[dt]['zAvg'] < splits['z'][2] and data[dt]['zAvg'] > splits['z'][1]:
            midiNotes.append([bt, 101, 120, nl])
        elif data[dt]['zAvg'] < splits['z'][3] and data[dt]['zAvg'] > splits['z'][2]:
            midiNotes.append([bt, 99, 112, nl])
        elif data[dt]['zAvg'] < splits['z'][4] and data[dt]['zAvg'] > splits['z'][3]:
            midiNotes.append([bt, 96, 104, nl])
        elif data[dt]['zAvg'] < splits['z'][5] and data[dt]['zAvg'] > splits['z'][4]:
            midiNotes.append([bt, 94, 96, nl])
        elif data[dt]['zAvg'] < splits['z'][6] and data[dt]['zAvg'] > splits['z'][5]:
            midiNotes.append([bt, 91, 88, nl])
        elif data[dt]['zAvg'] < splits['z'][7] and data[dt]['zAvg'] > splits['z'][6]:
            midiNotes.append([bt, 89, 80, nl])
        elif data[dt]['zAvg'] < splits['z'][8] and data[dt]['zAvg'] > splits['z'][7]:
            midiNotes.append([bt, 87, 72, nl])
        elif data[dt]['zAvg'] < splits['z'][9] and data[dt]['zAvg'] > splits['z'][8]:
            midiNotes.append([bt, 84, 64, nl])
        elif data[dt]['zAvg'] < splits['z'][10] and data[dt]['zAvg'] > splits['z'][9]:
            midiNotes.append([bt, 70, 56, nl])
        elif data[dt]['zAvg'] < splits['z'][11] and data[dt]['zAvg'] > splits['z'][10]:
            midiNotes.append([bt, 67, 48, nl])
        elif data[dt]['zAvg'] < splits['z'][12] and data[dt]['zAvg'] > splits['z'][11]:
            midiNotes.append([bt, 65, 40, nl])
        elif data[dt]['zAvg'] < splits['z'][13] and data[dt]['zAvg'] > splits['z'][12]:
            midiNotes.append([bt, 63, 32, nl])
        elif data[dt]['zAvg'] < splits['z'][14] and data[dt]['zAvg'] > splits['z'][13]:
            midiNotes.append([bt, 60, 24, nl])
        elif data[dt]['zAvg'] < splits['z'][15] and data[dt]['zAvg'] > splits['z'][14]:
            midiNotes.append([bt, 58, 16, nl])
        elif data[dt]['zAvg'] > splits['z'][15]:
            midiNotes.append([bt, 55, 8, nl])
        else:
            midiNotes.append([bt, 67, 1, nl])
        bt+=.125

    myMidi.add_track(midiNotes)
    myMidi.save_midi()


if __name__ == "__main__":
    files = ['4mu_hits', '4e_event', '4e_hits', '4mu_event', 'sp_event', 'sp_hits', 'susy13_hits', 'susy13_event', 'zmumu13_hits', 'zmumu13_event']
    #minorPentatonic = [36, 39, 41, 43, 46]
    #majorPentatonic = [36, 38, 40, 43, 45]
    for filename in files:
        #print "before getData"
        data = getData(filename)
        #print "before stdMinMax"
        stdMinMax = getStdMinMax(data)
        #print 'before zoneSplits'
        zoneSplits = getZoneSplits(stdMinMax)

        makeMidiNotes(zoneSplits, data, filename)
        print filename, "completed"

