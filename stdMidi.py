#!/usr/bin/env python2.7


''' 
Takes the standard deviation of x, y, z at each datatime
Gets the mins and maxes of those standard deviations
splits the mins and maxes into a certain number of sections

#TODO
assign the notes based on the ranges of the standard devations



Make a list that you pass into the make midi notes
have the list be the scale
have different scales that you can pass in
something like : scale[1] scale[2] in the function


'''
import sys
import os
import math
import numpy as np
from miditime.miditime import MIDITime

def getData(filename):
    datatimeStarted = False
    myFile = open('data/' + filename + '.speck')
    data = {}
    first = True
    for line in myFile:
        if not line.split():
            continue
        if line.split()[0] == 'datatime':
            if not first and len(data[currDataTime]['x']) != 0:
                data[currDataTime]['xAvg'] = np.std(data[currDataTime]['x'])
                data[currDataTime]['yAvg'] = np.std(data[currDataTime]['y'])
                data[currDataTime]['zAvg'] = np.std(data[currDataTime]['z'])
                print 'dt: {}, x: {}, y: {}, z: {}'.format(currDataTime, data[currDataTime]['xAvg'], data[currDataTime]['yAvg'], data[currDataTime]['zAvg'])
                #makeMidiNotes(midiNotes, data, currDataTime)
            first = False
            # start new datatime and initialize values
            datatimeStarted = True
            currDataTime = int(line.split()[1])
            data[currDataTime] = {}
            data[currDataTime]['x'] = np.array([])
            data[currDataTime]['y'] = np.array([])
            data[currDataTime]['z'] = np.array([])
            data[currDataTime]['xAvg'] = 0
            data[currDataTime]['yAvg'] = 0
            data[currDataTime]['zAvg'] = 0
            continue
        if not datatimeStarted or line.split()[0] == '#':
            continue
        currLine = line.rstrip('\n').split()
        x = float(currLine[0])
        y = float(currLine[1])
        z = float(currLine[2])
        data[currDataTime]['x'] = np.append(data[currDataTime]['x'],x)
        data[currDataTime]['y'] = np.append(data[currDataTime]['y'],y)
        data[currDataTime]['z'] = np.append(data[currDataTime]['z'],z)

    # get averages and append midi by last datatime
    if not first and len(data[currDataTime]['x']) != 0:
        data[currDataTime]['xAvg'] = np.std(data[currDataTime]['x'])
        data[currDataTime]['yAvg'] = np.std(data[currDataTime]['y'])
        data[currDataTime]['zAvg'] = np.std(data[currDataTime]['z'])
    myFile.close()
    return data

# generator to give floats based on a jump
def frange(x, y, jump):
    while x <= y:
        yield x
        x += jump

def getZoneSplits(myList):
    # get ranges for each axis
    xRange = [myList[0], myList[1]]
    yRange = [myList[2], myList[3]]
    zRange = [myList[4], myList[5]]
    splits = {'x':[],'y':[],'z':[]}

    # break the x, y, and z up
    for x in frange(xRange[0], xRange[1], (abs(xRange[0]) + abs(xRange[1]))/16):
        splits['x'].append(x)

    for x in frange(yRange[0], yRange[1], (abs(yRange[0]) + abs(yRange[1]))/16):
        splits['y'].append(x)

    for x in frange(zRange[0], zRange[1], (abs(zRange[0]) + abs(zRange[1]))/16):
        splits['z'].append(x)

    return splits

def getStdMinMax(data):
    xmin=xmax=ymin=ymax=zmin=zmax = 0
    for dt in data.keys():
        if data[dt]['xAvg'] < xmin:
            xmin = data[dt]['xAvg']
        elif data[dt]['xAvg'] > xmax:
            xmax = data[dt]['xAvg']
        elif data[dt]['yAvg'] < ymin:
            ymin = data[dt]['yAvg']
        elif data[dt]['yAvg'] > ymax:
            ymax = data[dt]['yAvg']
        elif data[dt]['zAvg'] < zmin:
            zmin = data[dt]['zAvg']
        elif data[dt]['zAvg'] > zmax:
            zmax = data[dt]['zAvg']
    return [xmin, xmax, ymin, ymax, zmin, zmax]

def makeMidiNotes(splits, data, filename):
    myMidi = MIDITime(120, 'music/' + filename + '.mid')
    midiNotes = []
    bt = 0
    nl = .125
    for dt in data.keys():
        if data[dt]['xAvg'] < splits['x'][1] and data[dt]['xAvg'] > splits['x'][0]:
            midiNotes.append([bt, 36, 127, nl])
        elif data[dt]['xAvg'] < splits['x'][2] and data[dt]['xAvg'] > splits['x'][1]:
            midiNotes.append([bt, 39, 120, nl])
        elif data[dt]['xAvg'] < splits['x'][3] and data[dt]['xAvg'] > splits['x'][2]:
            midiNotes.append([bt, 41, 112, nl])
        elif data[dt]['xAvg'] < splits['x'][4] and data[dt]['xAvg'] > splits['x'][3]:
            midiNotes.append([bt, 43, 104, nl])
        elif data[dt]['xAvg'] < splits['x'][5] and data[dt]['xAvg'] > splits['x'][4]:
            midiNotes.append([bt, 46, 96, nl])
        elif data[dt]['xAvg'] < splits['x'][6] and data[dt]['xAvg'] > splits['x'][5]:
            midiNotes.append([bt, 48, 88, nl])
        elif data[dt]['xAvg'] < splits['x'][7] and data[dt]['xAvg'] > splits['x'][6]:
            midiNotes.append([bt, 51, 80, nl])
        elif data[dt]['xAvg'] < splits['x'][8] and data[dt]['xAvg'] > splits['x'][7]:
            midiNotes.append([bt, 53, 72, nl])
        elif data[dt]['xAvg'] < splits['x'][9] and data[dt]['xAvg'] > splits['x'][8]:
            midiNotes.append([bt, 55, 64, nl])
        elif data[dt]['xAvg'] < splits['x'][10] and data[dt]['xAvg'] > splits['x'][9]:
            midiNotes.append([bt, 58, 56, nl])
        elif data[dt]['xAvg'] < splits['x'][11] and data[dt]['xAvg'] > splits['x'][10]:
            midiNotes.append([bt, 60, 48, nl])
        elif data[dt]['xAvg'] < splits['x'][12] and data[dt]['xAvg'] > splits['x'][11]:
            midiNotes.append([bt, 63, 40, nl])
        elif data[dt]['xAvg'] < splits['x'][13] and data[dt]['xAvg'] > splits['x'][12]:
            midiNotes.append([bt, 65, 32, nl])
        elif data[dt]['xAvg'] < splits['x'][14] and data[dt]['xAvg'] > splits['x'][13]:
            midiNotes.append([bt, 67, 24, nl])
        elif data[dt]['xAvg'] < splits['x'][15] and data[dt]['xAvg'] > splits['x'][14]:
            midiNotes.append([bt, 70, 16, nl])
        elif data[dt]['xAvg'] > splits['x'][15]:
            midiNotes.append([bt, 72, 8, nl])
        else:
            midiNotes.append([bt, 67, 1, nl])

#offset y note pitches by 4 compared to x for a major third interval(start at E)

        if data[dt]['yAvg'] < splits['y'][1] and data[dt]['yAvg'] > splits['y'][0]:
            midiNotes.append([bt, 51, 127, nl])
        elif data[dt]['yAvg'] < splits['y'][2] and data[dt]['yAvg'] > splits['y'][1]:
            midiNotes.append([bt, 53, 120, nl])
        elif data[dt]['yAvg'] < splits['y'][3] and data[dt]['yAvg'] > splits['y'][2]:
            midiNotes.append([bt, 55, 112, nl])
        elif data[dt]['yAvg'] < splits['y'][4] and data[dt]['yAvg'] > splits['y'][3]:
            midiNotes.append([bt, 58, 104, nl])
        elif data[dt]['yAvg'] < splits['y'][5] and data[dt]['yAvg'] > splits['y'][4]:
            midiNotes.append([bt, 60, 96, nl])
        elif data[dt]['yAvg'] < splits['y'][6] and data[dt]['yAvg'] > splits['y'][5]:
            midiNotes.append([bt, 63, 88, nl])
        elif data[dt]['yAvg'] < splits['y'][7] and data[dt]['yAvg'] > splits['y'][6]:
            midiNotes.append([bt, 65, 80, nl])
        elif data[dt]['yAvg'] < splits['y'][8] and data[dt]['yAvg'] > splits['y'][7]:
            midiNotes.append([bt, 67, 72, nl])
        elif data[dt]['yAvg'] < splits['y'][9] and data[dt]['yAvg'] > splits['y'][8]:
            midiNotes.append([bt, 70, 64, nl])
        elif data[dt]['yAvg'] < splits['y'][10] and data[dt]['yAvg'] > splits['y'][9]:
            midiNotes.append([bt, 72, 56, nl])
        elif data[dt]['yAvg'] < splits['y'][11] and data[dt]['yAvg'] > splits['y'][10]:
            midiNotes.append([bt, 75, 48, nl])
        elif data[dt]['yAvg'] < splits['y'][12] and data[dt]['yAvg'] > splits['y'][11]:
            midiNotes.append([bt, 77, 40, nl])
        elif data[dt]['yAvg'] < splits['y'][13] and data[dt]['yAvg'] > splits['y'][12]:
            midiNotes.append([bt, 79, 32, nl])
        elif data[dt]['yAvg'] < splits['y'][14] and data[dt]['yAvg'] > splits['y'][13]:
            midiNotes.append([bt, 82, 24, nl])
        elif data[dt]['yAvg'] < splits['y'][15] and data[dt]['yAvg'] > splits['y'][14]:
            midiNotes.append([bt, 84, 16, nl])
        elif data[dt]['yAvg'] > splits['y'][15]:
            midiNotes.append([bt, 87, 8, nl])
        else:
            midiNotes.append([bt, 67, 1, nl])

        #z values start at G
        if data[dt]['zAvg'] < splits['z'][1] and data[dt]['zAvg'] > splits['z'][0]:
            midiNotes.append([bt, 67, 127  , nl])
        elif data[dt]['zAvg'] < splits['z'][2] and data[dt]['zAvg'] > splits['z'][1]:
            midiNotes.append([bt, 70, 120, nl])
        elif data[dt]['zAvg'] < splits['z'][3] and data[dt]['zAvg'] > splits['z'][2]:
            midiNotes.append([bt, 72, 112, nl])
        elif data[dt]['zAvg'] < splits['z'][4] and data[dt]['zAvg'] > splits['z'][3]:
            midiNotes.append([bt, 75, 104, nl])
        elif data[dt]['zAvg'] < splits['z'][5] and data[dt]['zAvg'] > splits['z'][4]:
            midiNotes.append([bt, 77, 96, nl])
        elif data[dt]['zAvg'] < splits['z'][6] and data[dt]['zAvg'] > splits['z'][5]:
            midiNotes.append([bt, 79, 88, nl])
        elif data[dt]['zAvg'] < splits['z'][7] and data[dt]['zAvg'] > splits['z'][6]:
            midiNotes.append([bt, 82, 80, nl])
        elif data[dt]['zAvg'] < splits['z'][8] and data[dt]['zAvg'] > splits['z'][7]:
            midiNotes.append([bt, 84, 72, nl])
        elif data[dt]['zAvg'] < splits['z'][9] and data[dt]['zAvg'] > splits['z'][8]:
            midiNotes.append([bt, 87, 64, nl])
        elif data[dt]['zAvg'] < splits['z'][10] and data[dt]['zAvg'] > splits['z'][9]:
            midiNotes.append([bt, 89, 56, nl])
        elif data[dt]['zAvg'] < splits['z'][11] and data[dt]['zAvg'] > splits['z'][10]:
            midiNotes.append([bt, 91, 48, nl])
        elif data[dt]['zAvg'] < splits['z'][12] and data[dt]['zAvg'] > splits['z'][11]:
            midiNotes.append([bt, 94, 40, nl])
        elif data[dt]['zAvg'] < splits['z'][13] and data[dt]['zAvg'] > splits['z'][12]:
            midiNotes.append([bt, 96, 32, nl])
        elif data[dt]['zAvg'] < splits['z'][14] and data[dt]['zAvg'] > splits['z'][13]:
            midiNotes.append([bt, 99, 24, nl])
        elif data[dt]['zAvg'] < splits['z'][15] and data[dt]['zAvg'] > splits['z'][14]:
            midiNotes.append([bt, 101, 16, nl])
        elif data[dt]['zAvg'] > splits['z'][15]:
            midiNotes.append([bt, 103, 8, nl])
        else:
            midiNotes.append([bt, 67, 1, nl])
        bt+=.125

    myMidi.add_track(midiNotes)
    myMidi.save_midi()


if __name__ == "__main__":
    #files = ['hits', 'sp_hits', 'susy13_hits', 'zmumu13_hits']
    #files = ['4mu_hits', '4e_event', '4e_hits', '4mu_event', 'sp_event', 'sp_hits', 'susy13_hits', 'susy13_event', 'zmumu13_hits', 'zmumu13_event']
    files = ['sp_event', 'sp_hits', 'susy13_hits', 'susy13_event', 'zmumu13_hits', 'zmumu13_event']
    #minorPentatonic = [36, 39, 41, 43, 46]
    #majorPentatonic = [36, 38, 40, 43, 45]
    for filename in files:
        print "before getData"
        data = getData(filename)
        print "before stdMinMax"
        stdMinMax = getStdMinMax(data)
        print 'before zoneSplits'
        zoneSplits = getZoneSplits(stdMinMax)

        makeMidiNotes(zoneSplits, data, filename)
        print filename, "completed"

